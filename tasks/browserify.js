/*
 * grunt-browserify
 * https://github.com/pix/grunt-browserify
 *
 * Copyright (c) 2012 Camille Moncelier
 * Licensed under the MIT license.
 */

module.exports = function (grunt) {
  'use strict';
  // Please see the grunt documentation for more information regarding task and
  // helper creation: https://github.com/gruntjs/grunt/wiki/Creating-tasks
  // ==========================================================================
  // TASKS
  // ==========================================================================
  grunt.registerMultiTask('browserify', 'Your task description goes here.', function () {

    var helpers = require('grunt-lib-legacyhelpers').init(grunt);
    var done = this.async();
    var browserify = require('browserify'),
        b = browserify(),
        files, src;
    if (this.data.beforeHook) {
      this.data.beforeHook.call(this, b);
    }

    (this.data.ignore || []).forEach(function (filepath) {
      grunt.verbose.writeln('Ignoring "' + filepath + '"');
      b.ignore(filepath);
    });

    (this.data.requires || []).forEach(function (req) {
      grunt.verbose.writeln('Adding "' + req + '" to the required module list');
      b.require(req);
    });
    
    grunt.file.expand({}, this.data.src || []).forEach(function (filepath) {
      grunt.verbose.writeln('Adding "' + filepath + '" to the entry file list');
      b.add(filepath);
    });


    if (this.data.hook) {
      this.data.hook.call(this, b);
    }
    var self = this;
    b.bundle(this.data.options || {}, function(err, src){
      if(err) throw err;
      grunt.file.write(self.data.dest || self.target, src);
      done();
    });
    
  });

};
